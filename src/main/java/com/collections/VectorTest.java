package com.collections;

import java.util.Vector;

/**
 */
public class VectorTest {
    public static void main(String[] args) {
        Vector<Integer> vector = new Vector<Integer>();
        for (int i=0; i<10; i++) {
            vector.add(i);
        }
        System.out.println("vector: " + vector.toString());
        for (int i=0; i<vector.size(); i++) {
            vector.remove(i);
            System.out.println("after remove index at " + i + ", vector's size is "+vector.size() +" vector's hash code is "+ vector.hashCode()+", vector: " + vector.toString());
        }
    }
}