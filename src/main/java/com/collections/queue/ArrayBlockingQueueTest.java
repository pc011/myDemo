package com.collections.queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

/**
 */
public class ArrayBlockingQueueTest {

    public static void main(String[] args) {
        final ConcurrentLinkedQueue<Integer> queue = new ConcurrentLinkedQueue();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i<10;i++) {
                    queue.offer(i);
                    System.out.println(Thread.currentThread().getName() + ": " + queue.toString());
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i<10;i++) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    queue.poll();
                    System.out.println(Thread.currentThread().getName() + ": " + queue.toString());
                }
            }
        }).start();
    }

}
