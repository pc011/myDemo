package com.collections.queue;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

public class QueueTest {

    public static void main(String[] args) {

        ConcurrentLinkedQueue<Integer> arrayQueue = new ConcurrentLinkedQueue<Integer>();
        for (int i=0; i< 20; i++) {
            arrayQueue.add((int) (Math.random() * 100));
        }

        Iterator<Integer> iterator = arrayQueue.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + ", ");
        }
    }

}
