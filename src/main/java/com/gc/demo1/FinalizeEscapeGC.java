package com.gc.demo1;

/**
 * 此代码演示两点
 * 1 对象可以在被GC时自我拯救
 * 2 这种被拯救的机会只有一次，因为一个对象的finalize()方法最多只会被系统自动调用一次
 */
public class FinalizeEscapeGC {

    public static FinalizeEscapeGC SAVE_HOOK = null;

    public void stillActive() {
        System.out.println("Yes, I'm still alive");
    }

    //对象真正死亡至少要经历两次标记过程，第一次是在当对象不可从GC Roots达时，此时如果对象覆盖了finalize方法
    //对象则会被放置在F-Queue队列中，并在稍后通过虚拟机创建的低优先级的finalize线程去执行它（触发finalize方法）
    //如果对象在finalize方法中重新与引用链上的对象进行关联，则在第二次标记时，它将被移出“即将回收”集合。
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("finalize method executed");
        FinalizeEscapeGC.SAVE_HOOK = this;
        System.out.println("My hashCode is "+FinalizeEscapeGC.SAVE_HOOK.hashCode());
    }

    public static void main(String[] args) throws Throwable {
        SAVE_HOOK = new FinalizeEscapeGC();
        System.out.println("My hashCode is "+SAVE_HOOK.hashCode());

        //对象第一次在finalize中成功自救，但不建议使用这种方式
        SAVE_HOOK = null;
        System.gc();
        Thread.sleep(500);
        if (SAVE_HOOK != null) {
            SAVE_HOOK.stillActive();
        } else {
            System.out.println("No, I'm dead");
        }

        //对象无法再次自救，每个对象的finalize方法只会被执行一次
        SAVE_HOOK = null;
        System.gc();
        Thread.sleep(500);
        if (SAVE_HOOK != null) {
            SAVE_HOOK.stillActive();
        } else {
            System.out.println("No, I'm dead");
        }
    }
}
