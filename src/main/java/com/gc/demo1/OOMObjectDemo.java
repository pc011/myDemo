package com.gc.demo1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
/**
 */
public class OOMObjectDemo {

    static class OOMObject {
        public byte[] placeholder = new byte[64 * 1024];
    }

    /**
     * VM Args: -Xms100M -Xmx100M -XX:+UseSerialGC
     * @param num
     * @throws InterruptedException
     */
    public static void fillHeap(int num) throws InterruptedException {
        List<OOMObject> list = new ArrayList();
        for (int i=0;i<num;i++) {
            Thread.sleep(50);
            list.add(new OOMObject());
        }
        System.gc();
    }

    /**
     * 线程死循环演示
     */
    public static void createBusyThread() {
        Thread thread = new Thread(new Runnable() {
            public void run() {
                while (true);
            }
        }, "TestBusyThread");
        thread.start();
    }

    /**
     * 线程锁等待演示
     */
    public static void createLockThread(final Object lock) {
        Thread thread = new Thread(new Runnable() {
            public void run() {
                synchronized (lock) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, "TestLockThread");
        thread.start();
    }

    public static void main(String[] args) throws InterruptedException, IOException {
//        Thread.sleep(10000);
//        fillHeap(2000);

//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//        br.readLine();
//        createBusyThread();
//        br.readLine();
//
//        Object obj = new Object();
//        createLockThread(obj);

        Thread.sleep(10000);
        for (int i=0; i<1000000; i++) {
            //new Thread(new SynAddRunnable(1, 2), "Thread A"+i).start();
            //new Thread(new SynAddRunnable(2, 1),"Thread B"+i).start();
            Object obj = new Object();
            createLockThread(obj);
        }
    }

    /**
     * 线程死锁等待演示
     */
    static class SynAddRunnable implements Runnable {

        int a,b;
        public SynAddRunnable(int a, int b) {
            this.a = a;
            this.b = b;
        }

        public void run() {
            synchronized (Integer.valueOf(a)) {
                synchronized (Integer.valueOf(b)) {
                    System.out.println(a + b);
                }
            }
        }
    }

}
