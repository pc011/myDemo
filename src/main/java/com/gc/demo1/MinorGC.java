package com.gc.demo1;

public class MinorGC {

    public static final int _1MB = 1024 * 1024;

    public static void main(String[] args) throws InterruptedException {
        //testPretenureSizeThreshold();
        testTenuringThreshold();
    }

    /**
     * 大对象直接进入老年代
     * VM Args: -XX:+UseSerialGC -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8
     * -XX:PretenureSizeThreshold=3145728
     */
    public static void testPretenureSizeThreshold() {
        byte[] allocation = new byte[4 * _1MB];
    }

    /**
     * VM Args: -XX:+UseSerialGC -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8
     * -XX:MaxTenuringThreshold=1
     * -XX:+PrintTenuringDistribution
     * 长期存活的对象进入老年代
     */
    public static void testTenuringThreshold(){
        byte[] allocation1,allocation2,allocation3;
        allocation1 = new byte[_1MB / 4];
        allocation2 = new byte[4 * _1MB];
        allocation3 = new byte[4 * _1MB];
        allocation3 = null;
        allocation3 = new byte[4 * _1MB];
    }

    /**
     * VM Args: -XX:+UseSerialGC -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8
     * -XX:MaxTenuringThreshold=1
     * -XX:+PrintTenuringDistribution
     * 动态对象年龄判定
     */
    public static void testTenuringThreshold2(){
        byte[] allocation1,allocation2,allocation3,allocation4;
        allocation1 = new byte[_1MB / 4];
        //allocation1+allocation2大于Survior空间的一半
        allocation2 = new byte[_1MB / 4];
        allocation3 = new byte[4 * _1MB];
        allocation4 = new byte[4 * _1MB];
        allocation4 = null;
        allocation4 = new byte[4 * _1MB];
    }

    /**
     * 空间分配担保，HandlePromotionFailure JDK6 Update24版本之前有效，忽略
     */
    public static void testHandlePromotionFailure() {

    }

    /**
     * 触发Minor GC
     * VM Args: -XX:+PrintGCDetails -Xms20M -Xmx20M -Mmn10M -XX:+UseSerialGC
     */
    public static void testAllocation(){

        byte[] allocation1, allocation2, allocation3, allocation4;
        allocation1 = new byte[2*_1MB];
        allocation2 = new byte[2*_1MB];
        //出现一次Minor GC
        allocation3 = new byte[2*_1MB];
        allocation4 = new byte[4*_1MB];
    }

}
