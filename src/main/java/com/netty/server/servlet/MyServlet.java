package com.netty.server.servlet;

import com.netty.server.http.GPHttpRequest;
import com.netty.server.http.GPHttpResponse;
import com.netty.server.http.GPServlet;

import java.io.UnsupportedEncodingException;

public class MyServlet extends GPServlet {

    @Override
    public void doGet(GPHttpRequest request, GPHttpResponse response) {
        try {
            response.write("name:"+request.getParameter("name"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(GPHttpRequest request, GPHttpResponse response) {
        doGet(request, response);
    }
}
