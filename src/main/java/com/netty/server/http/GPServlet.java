package com.netty.server.http;

public abstract class GPServlet {

    public abstract void doGet(GPHttpRequest request, GPHttpResponse response);

    public abstract void doPost(GPHttpRequest request, GPHttpResponse response);

}
