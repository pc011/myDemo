package com.netty.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;

public class GPTomcat {

    public void start(int port) throws Exception {
        //BOSS线程
        EventLoopGroup parentGroup = new NioEventLoopGroup();
        //Worker线程
        EventLoopGroup childGroup = new NioEventLoopGroup();
        try {
            //Netty服务
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            //主从Reactor多线程模型
            serverBootstrap.group(parentGroup, childGroup)
                    //主线程处理类（处理连接请求）
                    .channel(NioServerSocketChannel.class)
                    //子线程处理类（处理读写和编解码）
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                @Override
                //当客户端连接时，触发事件
                protected void initChannel(SocketChannel client) throws Exception {
                    //业务逻辑链路

                    client.pipeline()
                            .addLast(new HttpResponseEncoder()) //编码器
                            .addLast(new HttpRequestDecoder()) //解码器
                            .addLast(new GPTomcatHandler()); //业务逻辑处理

                }})
                    //主线程配置
                    .option(ChannelOption.SO_BACKLOG, 128) //分配线程的最大数量128
                    //子线程配置
                    .childOption(ChannelOption.SO_KEEPALIVE, true); //长连接

            //sync同步阻塞
            ChannelFuture channelFuture = serverBootstrap.bind(port).sync();
            System.out.println("GP Tomcat已启动");
            channelFuture.channel().closeFuture().sync();

        } finally {
            parentGroup.shutdownGracefully();
            childGroup.shutdownGracefully();
        }
    }

    public static void main(String[] args) {
        try {
            new GPTomcat().start(8080);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
