package com.netty.server;

import com.netty.server.http.GPHttpRequest;
import com.netty.server.http.GPHttpResponse;
import com.netty.server.servlet.MyServlet;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.util.concurrent.EventExecutorGroup;

public class GPTomcatHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof HttpRequest) {
            HttpRequest request = (HttpRequest)msg;

            GPHttpRequest gpHttpRequest = new GPHttpRequest(ctx, request);
            GPHttpResponse gpHttpResponse = new GPHttpResponse(ctx, request);

            new MyServlet().doGet(gpHttpRequest, gpHttpResponse);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
    }
}
