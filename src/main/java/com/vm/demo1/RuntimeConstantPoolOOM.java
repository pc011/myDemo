package com.vm.demo1;

import java.util.ArrayList;
import java.util.List;

/**
 * VM Args: -XX:PermSize=10M -XX:MaxPermSize=10M
 */
public class RuntimeConstantPoolOOM {

    public static void main(String[] args) {
        //使用list保持常量池的引用，避免FULL GC回收常量池的行为
        List<String> list = new ArrayList<String>();

        //intern 如果字符串常量池中包含该字符串，则返回该对象，否则将该对象包含的字符串加入字符串常量池
        int i=0;
        while(true) {
            list.add(String.valueOf(i++).intern());
        }
    }
}