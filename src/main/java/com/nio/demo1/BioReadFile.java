package com.nio.demo1;

import java.io.*;

public class BioReadFile {

    public static void main(String[] args) {
        try {
            FileInputStream fis = new FileInputStream(new File("D://io.txt"));
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            System.out.println(br.readLine().toString());
            System.out.println(br.readLine().toString());
            System.out.println(br.readLine().toString());
            System.out.println(br.readLine().toString());
            br.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
