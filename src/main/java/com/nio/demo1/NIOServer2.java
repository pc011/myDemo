package com.nio.demo1;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class NIOServer2 {

    private int port = 8080;

    private InetSocketAddress address = null;

    private Selector selector;

    public NIOServer2(int port) {
        this.port = port;
        address = new InetSocketAddress(this.port);

        try {
            ServerSocketChannel channel = ServerSocketChannel.open();
            channel.bind(address);
            channel.configureBlocking(false);

            selector = Selector.open();
            channel.register(selector, SelectionKey.OP_ACCEPT);

            System.out.println("服务器准备就绪，监听端口是："+this.port);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws IOException {
        new NIOServer2(8080).listener();

    }

    private void listener() throws IOException {
        while (true) {
            int wait = this.selector.select();
            if (wait == 0) {
                continue;
            }
            Set<SelectionKey> keySet = this.selector.selectedKeys();
            Iterator<SelectionKey> iterator = keySet.iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                process(key);
                iterator.remove();
            }
        }
    }

    private void process(SelectionKey key) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        if (key.isAcceptable()) {
            ServerSocketChannel server = (ServerSocketChannel) key.channel();
            SocketChannel client = server.accept();
            client.configureBlocking(false);
            client.register(selector, SelectionKey.OP_READ);
        }
        if (key.isReadable()) {
            SocketChannel client = (SocketChannel) key.channel();
            int len = client.read(buffer);
            if (len > 0) {
                buffer.flip();
                String content = new String(buffer.array(), 0 ,len);
                client.register(selector, SelectionKey.OP_WRITE);
            }
            buffer.clear();
        }
        if (key.isWritable()) {
            SocketChannel client = (SocketChannel) key.channel();
            buffer = buffer.wrap("Hello World".getBytes());
            client.write(buffer);
        }

    }

}
