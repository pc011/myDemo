package com.nio.demo1;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.UUID;

public class BioClient {

    public static void main(String[] args) {
        Socket client = null;
        OutputStream os = null;
        try {
            client = new Socket("localhost", 8080);
            os = client.getOutputStream();
            String name = UUID.randomUUID().toString();
            os.write(name.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (client != null) {
                    client.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

}
