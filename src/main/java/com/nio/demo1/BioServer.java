package com.nio.demo1;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class BioServer {

    ServerSocket server;

    public BioServer(int port) {
        try {
            server = new ServerSocket(port);
            System.out.println("BIO服务已启动，监听端口是:"+port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void listener() {
        while (true) {
            Socket client = null;
            try {
                client = server.accept();
                InputStream is = client.getInputStream();
                //缓冲区
                byte[] buff = new byte[1024];
                int len = is.read(buff);
                if (len > 0) {
                    System.out.println("收到:" + new String(buff, 0, len));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    public static void main(String[] args) {

        new BioServer(8080).listener();
    }

}
