package com.nio.demo1;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Set;

public class NIOServer {

    private int port = 8080;

    private Charset charset = Charset.forName("utf-8");

    private Selector selector;

    public NIOServer(int port) throws IOException {
        this.port = port;

        //打开通道
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();

        //设置端口
        serverSocketChannel.bind(new InetSocketAddress(this.port));
        //设置为非阻塞模式
        serverSocketChannel.configureBlocking(false);

        //打开阀门
        selector = Selector.open();
        //设置开启监听
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

        System.out.println("服务已启动，监听端口是:"+this.port);
    }

    public void listener() throws IOException {
        while (true) {
            //获取正在等待的任务的数量
            int wait = selector.select();
            if (wait == 0) {
                continue;
            }
            //获取等待的所有的任务
            Set<SelectionKey> keys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = keys.iterator();
            while(iterator.hasNext()) {
                SelectionKey key = iterator.next();
                //删除已取出的任务
                iterator.remove();
                //处理任务
                process(key);
            }
        }

    }

    private void process(SelectionKey key) throws IOException {
        //判断客户端是否可以连接
        if (key.isAcceptable()) {
            ServerSocketChannel server = (ServerSocketChannel) key.channel();
            SocketChannel client = server.accept();
            //非阻塞模式
            client.configureBlocking(false);
            //注册选择器，并设置为读取模式，收到一个连接请求，然后开启一个SocketChannel并注册到selector上
            client.register(selector, SelectionKey.OP_READ);
            //将此对应的channel设置为准备接受其他客户端的请求
            key.interestOps(SelectionKey.OP_ACCEPT);
            client.write(charset.encode("请输入与昵称"));
        }
        //处理来自客户端的数据读取请求
        if (key.isReadable()) {
            //返回该SelectionKey对应的Channel，其中有数据需要读取
            SocketChannel client = (SocketChannel) key.channel();
            //往缓冲区中读数据
            ByteBuffer buff = ByteBuffer.allocate(1024);
            StringBuffer content = new StringBuffer();
            try {
                while (client.read(buff) > 0) {
                    buff.flip();
                    content.append(charset.decode(buff));
                }
                //将此对应的channel设置为准备下一次接受数据
                key.interestOps(SelectionKey.OP_READ);
            } catch (IOException e) {
                key.cancel();
                if (key.channel() != null) {
                    key.channel().close();
                }
            }
//            if (content.length() > 0) {
//                String[] arrayContent = content.toString().split("");
//
//            }
        }

    }

}
