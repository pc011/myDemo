package com.nio.demo1;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;

/**
 */
public class FileChannelDemo {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        read2();
        System.out.println("读取完毕，耗时" + (System.currentTimeMillis()-start)/1000 +" second");
    }

    public static void read2() {
        try {
            int port = 9999;
            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel.socket().bind(new InetSocketAddress(port));
            Selector selector = Selector.open();
            SelectionKey selectionKey = serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            int interestSet = selectionKey.interestOps();
            boolean isAccept = (interestSet & SelectionKey.OP_ACCEPT) == SelectionKey.OP_ACCEPT;
            System.out.println("isAccept:"+ isAccept);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void read() {
        try {
            RandomAccessFile logFile = new RandomAccessFile("D:/log1.log", "rw");

            FileChannel inChannel = logFile.getChannel();
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            int bytesRead = inChannel.read(buffer);
            while (bytesRead != -1) {
                System.out.println("Read " + bytesRead);
                buffer.flip();
                while(buffer.hasRemaining()) {
                    System.out.print((char)buffer.get());
                }
                buffer.clear();
                bytesRead = inChannel.read(buffer);
            }
            logFile.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
