package com.thread.chapter03;

/**
 * 非线程安全的可变整数类
 * get和set方法不同步，可能出现读取到失效的数据
 *
 * 最低安全性：当线程在没有同步的情况下读取变量时，可能会得到一个失效的值，但这个值至少是由之前某个线程设置的值，而不是一个随机值
 * 非volitate类型的64位数值变量无法保证最低安全性
 */
public class MutableInteger {

    private int value;

    public int get() {
        return value;
    }

    public void set(int value) {
        this.value = value;
    }

}
