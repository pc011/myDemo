package com.thread.chapter03;

/**
 * 线程安全的可变整数类,防止出现失效的值
 */
public class SynchronizedInteger {

    private int value;

    public synchronized int get() {
        return value;
    }

    public synchronized void set(int value) {
        this.value = value;
    }
}
