package com.thread.chapter03;

import com.thread.chapter02.demo1.MyAbstractServlet;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.math.BigInteger;

/**
 * 使用指向不可变容器对象的volatile类型引用以缓存最新的结果
 * 通过包含多个状态变量的容器对象来维持不变性条件
 * 使用一个volatile类型的引用来确保可见性
 * 使得VolatileCachedFactorizer在没有显式使用锁的情况下任然是线程安全的
 */
public class VolatileCachedFactorizer extends MyAbstractServlet {

    private volatile OneValueCache cache = new OneValueCache(null, null);

    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        BigInteger i = extractFromRequest(request);
        BigInteger[] factors = cache.getFactors(i);
        if (factors == null) {
            factors = factor[i.intValue()];
            cache = new OneValueCache(i, factors);
        }
        encodeIntoResponse(response, factors);
    }



}
