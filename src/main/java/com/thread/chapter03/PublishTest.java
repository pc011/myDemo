package com.thread.chapter03;

import java.util.EventListener;
import java.util.HashSet;
import java.util.Set;

/**
 * Secret对象被发布
 * 对象的引用保存在公有的静态变量中，任何类和线程都能看见该对象，Set中保存的对象也被发布
 */
public class PublishTest {

    public static Set<Secret> knownSecrets;

    public void initialize() {
        knownSecrets = new HashSet<Secret>();
    }

    private class Secret {
    }
}

/**
 * 内部的可变状态states逸出，私有的变量被发布
 */
class UnsafeStates {
    private String[] states = new String[] {
        "a", "b", "c"
    };

    public String[] getStates() {
        return states;
    }

}

/**
 * 隐式的使this引用溢出
 */
class ThisEscape {

//    public ThisEscape(EventSource source) {
//        source.registerListiner(
//                new EventListener() {
//                    public void onEvent(Event e) {
//                        doSomething(e);
//                    }
//                }
//        );
//    }

}