package com.thread.chapter04;

/**
 * 使用java监视器模式的线程安全计数器
 */
public final class Counter {

    private long value = 0;

    public synchronized long getValue() {
        return value;
    }

    public synchronized long increment() {
        if (value == Integer.MAX_VALUE) {
            throw new RuntimeException();
        }
        return ++value;
    }
}
