package com.thread.chapter04;

import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 将线程安全性委托给多个状态变量
 * 各个状态之间不存在耦合关系，VisualComponent将其线程安全性委托给keyListeners和mouseListeners
 *
 * 如果一个类由多个独立且线程安全的状态变量组成，并且在所有的操作中都不包含无效的状态转换，
 * 那么可以将线程安全性委托给底层的状态变量。
 *
 * 如果一个状态变量是线程安全的，并且没有任何不变性条件来、约束它的值，
 * 在变量的操作上也不存在任何不允许的状态转换，那么就可以安全的发布这个变量
 */
public class VisualComponent {

    private final List<KeyListener> keyListeners = new CopyOnWriteArrayList<>();
    private final List<MouseListener> mouseListeners = new CopyOnWriteArrayList<>();

    public void addKeyListener(KeyListener listener) {
        keyListeners.add(listener);
    }

    public void addMouseListener(MouseListener listener) {
        mouseListeners.add(listener);
    }

    public void removeKeyListener(KeyListener listener) {
        keyListeners.remove(listener);
    }

    public void removeMouseListener(MouseListener listener) {
        mouseListeners.remove(listener);
    }

}
