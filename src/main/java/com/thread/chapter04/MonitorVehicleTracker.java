package com.thread.chapter04;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 基于监视器模式的车辆追踪
 * 追踪器类是线程安全的，Map对象和Point对象都未被发布
 * 由于需要频繁的复制对象，在容量大的情况下将极大的降低性能
 */
public class MonitorVehicleTracker {

    private final Map<String, MutablePoint> locations;

    public MonitorVehicleTracker(Map<String, MutablePoint> locations) {
        this.locations = deepCopy(locations);
    }

    public synchronized Map<String, MutablePoint> getLocations() {
        return deepCopy(locations);
    }

    /**
     * 返回车辆位置快照
     * @param id
     * @return
     */
    public synchronized MutablePoint getLocation(String id) {
        MutablePoint location = locations.get(id);
        return location == null ? null : new MutablePoint(location);
    }

    public synchronized void setLocation(String id, int x, int y) {
        MutablePoint location = locations.get(id);
        if (location == null) {
            throw new RuntimeException();
        }
        location.x = x;
        location.y = y;
    }

    private static Map<String, MutablePoint> deepCopy(Map<String, MutablePoint> m) {
        Map<String, MutablePoint> result = new HashMap<>();
        for (String id : m.keySet()) {
            result.put(id, new MutablePoint(m.get(id)));
        }
        return Collections.unmodifiableMap(result);
    }

    /**
     * 可变Point类，非线程安全的，不建议使用
     */
    static class MutablePoint {

        private int x, y;

        public MutablePoint(MutablePoint p) {
            x = p.x;
            y = p.y;
        }

        public MutablePoint(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

}
