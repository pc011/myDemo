package com.thread.chapter04;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 基于委托的车辆跟踪器
 * 通过ConcurrentHashMap来管理对所有状态的访问
 */
public class DelegatingVehicleTracker {

    private final ConcurrentHashMap<String, Point> locations;

    private final Map<String, Point> unmodifiableMap;

    public DelegatingVehicleTracker(Map<String, Point> points) {
        this.locations = new ConcurrentHashMap<>(points);
        this.unmodifiableMap = Collections.unmodifiableMap(locations);
    }

    /**
     * 返回不可修改但却实时的车辆位置视图
     * @return
     */
    public Map<String, Point> getLocations() {
        return unmodifiableMap;
    }

    public Point getLocation(String id) {
        return locations.get(id);
    }

    public void setLocation(String id, int x, int y) {
        Point point = new Point(x, y);
        if (locations.replace(id, point) == null) {
            throw new RuntimeException();
        }
    }

    /**
     * 不可变Point类,线程安全
     */
    static class Point {

        public final int x, y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

}
