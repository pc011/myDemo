package com.thread.chapter04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListHelper<E> {

    public List<E> list = Collections.synchronizedList(new ArrayList<E>());

    //线程安全的通过客户端锁来实现“若没有则添加”，使用了正确的客户端加锁
    public boolean putIfAbsent(E x) {
        synchronized (list) {
            boolean absent = !list.contains(x);
            if (absent) {
                list.add(x);
            }
            return absent;
        }
    }

    //非线程安全的“若没有则添加”，由于在错误的锁上进行了同步
    public synchronized boolean putIfAbsent2(E x) {
        boolean absent = !list.contains(x);
        if (absent) {
            list.add(x);
        }
        return absent;
    }

}
