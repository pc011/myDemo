package com.thread.chapter04;

import java.util.List;

/**
 * 通过组合实现若没有则添加
 * ImprovedList通过自身的内置锁添加一层额外的加锁
 */
//public class ImprovedList<T> implements List<T> {
//
//    private final List<T> list;
//
//    public ImprovedList(List<T> list) {
//        this.list = list;
//    }
//
//    public synchronized boolean putIfAbsent(T x) {
//        boolean contains = list.contains(x);
//        if (contains) {
//            list.add(x);
//        }
//        return !contains;
//    }
//
//
//
//}
