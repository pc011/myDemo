package com.thread.chapter04;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 安全发布底层状态的车辆追踪器
 * PublishingVehicleTracker将其线程安全委托给底层的ConcurrentHashMap
 */
public class PublishingVehicleTracker {

    private final Map<String, SafePoint> locations;

    private final Map<String, SafePoint> unmodifiableMap;

    public PublishingVehicleTracker(Map<String, SafePoint> locations) {
        this.locations = new ConcurrentHashMap<>(locations);
        this.unmodifiableMap = Collections.unmodifiableMap(locations);
    }

    /**
     * 返回不可修改但却实时的车辆位置视图
     * @return
     */
    public Map<String, SafePoint> getLocations() {
        return unmodifiableMap;
    }

    public SafePoint getLocation(String id) {
        return locations.get(id);
    }

    public void setLocation(String id, int x, int y) {
        if (!locations.containsKey(id)) {
            throw new RuntimeException();
        }
        locations.get(id).set(x, y);
    }

    /**
     * 线程安全且可变的point类
     */
    static class SafePoint {

        private int x, y;

        private SafePoint(int[] a) {
            this.x = a[0];
            this.y = a[1];
        }

        public SafePoint(SafePoint p) {
            //如果拷贝构造函数实现为this(p.x, p.y)，那么会产生竞态条件，使用私有构造函数可以避免这种竞态条件
            this(p.get());
        }

        public synchronized int[] get() {
            return new int[]{x, y};
        }

        public synchronized void set(int x, int y) {
            this.x = x;
            this.y = y;
        }

    }

}
