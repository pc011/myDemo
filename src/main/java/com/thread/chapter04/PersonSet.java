package com.thread.chapter04;

import java.util.HashSet;
import java.util.Set;

/**
 * 通过封闭机制来确保线程安全
 * 封装机制更易于构造线程安全的类，因为当封闭类的状态时，在分析类的安全性时就无需检查整个程序
 */
public class PersonSet {

    private final Set<Person> mySet = new HashSet<>();

    public synchronized void addPerson(Person p) {
        mySet.add(p);
    }

    public synchronized boolean containsPerson(Person p) {
        return mySet.contains(p);
    }

    private class Person {
    }
}
