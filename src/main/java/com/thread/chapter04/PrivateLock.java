package com.thread.chapter04;

/**
 * 通过一个私有锁来保护状态
 */
public class PrivateLock {

    private final Object myLock = new Object();

    Widget widget;

    void someMethod() {
        synchronized (myLock) {
            //访问或修改widget的状态
        }
    }

    private class Widget {
    }
}
