package com.thread.chapter04;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * NumberRange类无法保证它的不变性条件。非线程安全的
 * 没有维持对上界和下界进行约束的不变形条件。
 */
public class NumberRange {

    private final AtomicInteger lower = new AtomicInteger(0);
    private final AtomicInteger upper = new AtomicInteger(0);

    public void setLower(int i) {
        //不安全的先检查后执行
        if (i > upper.get()) {
            throw new RuntimeException();
        }
        lower.set(i);
    }

    public void setUpper(int i) {
        if (i < lower.get()) {
            throw new RuntimeException();
        }
        upper.set(i);
    }

    public boolean isInRange(int i) {
        return i >= lower.get() && i <= upper.get();
    }

}
