package com.thread.chapter15.demo1;

/**
 * 基于CAS实现的非阻塞计数器
 */
public class CasCounter {

    private SimulatedCAS value;

    public int getValue() {
        return value.get();
    }

    public int increment() {
        int v;
        //使调用者处理竞争问题（包括重试，回退，放弃）
        do {
            v = value.get();
        } while (v != value.compareAndSwap(v, v + 1));
        return v + 1;
    }

}
