package com.thread.chapter15.demo2;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Michael-Scott非阻塞算法中的插入算法
 */
public class LinkedQueue<E> {

    private final Node<E> dummy = new Node<>(null, null);

    private final AtomicReference<Node<E>> head = new AtomicReference<>(dummy);
    private final AtomicReference<Node<E>> tail = new AtomicReference<>(dummy);

    /**
     * 如果队列处于稳定状态时，尾节点的next域将为空
     * 如果队列处于中间状态，tail.next将为非空
     * 因此任何线程都能通过检查tail.next来获取队列当前的状态
     * 当队列处于中间状态时，可以通过将尾节点向前移动一个节点，从而结束其它线程正在执行的插入操作，使得队列恢复为稳定状态
     * @param item
     * @return
     */
    public boolean put(E item) {
        Node<E> newTail = new Node<>(item, null);
        while (true) {
            Node<E> curTail = tail.get();
            Node<E> tailNext = curTail.next.get();
            if (curTail == tail.get()) {
                if (tailNext != null) {
                    //队列处于中间状态，推进尾节点
                    tail.compareAndSet(curTail, tailNext);
                } else {
                    //处于稳定状态，尝试插入新节点
                    if (curTail.next.compareAndSet(tailNext, newTail)) {
                        //插入操作成功，尝试推进尾节点
                        tail.compareAndSet(curTail, newTail);
                        return true;
                    }
                }
            }
        }

    }

    private static class Node<E> {

        private final E item;
        private final AtomicReference<Node<E>> next;

        private Node(E item, Node<E> next) {
            this.item = item;
            this.next = new AtomicReference<>(next);
        }
    }

}
