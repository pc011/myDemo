package com.thread.demo6;

public class ReferenceTest {

    public static void main(String[] args) {

        MyClass myClass = new MyClass();
        System.out.println("1-" + myClass.toString());
        changeClass(myClass);
        System.out.println("3-" + myClass.toString());

        String a = "abc";
        System.out.println("1-" + a);
        changeString(a);
        System.out.println("3-" + a);

        StringBuilder a1 = new StringBuilder("a");
        StringBuffer b1 = new StringBuffer("b");
        System.out.println(test1(0));


    }

    public static void add(StringBuilder a, StringBuffer b) {
        a.append(b);

    }

    public static void changeClass(MyClass myClass ) {
        //myClass =
        System.out.println("2-" + myClass.toString());

    }

    public static void changeString(String str) {
        str = "efg";
        System.out.println("2-"+str);
    }

    public static int test(int i) {
        try {
             ++i;
        } finally {
            ++i;
        }
        return ++i;
    }

    public static int test1(int i) {
        try {
            return ++i;
        } finally {
            ++i;
        }
    }

}



class MyClass {

    private int a = 0;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    @Override
    public String toString() {
        return super.toString() + "-a:" + a;
    }
}

