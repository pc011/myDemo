package com.thread.demo6;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamTest {

    public static void main(String[] args) {
        StreamTest st = new StreamTest();
        new Thread(new Runnable() {
            @Override
            public void run() {
                st.testA();
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                st.testB();
            }
        }).start();
    }

    public synchronized void testA() {
        System.out.println("testA-" + Thread.currentThread().getName()+" sleep start");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("testA-" + Thread.currentThread().getName()+" sleep end");
    }

    public void testB() {
        synchronized (this) {
            System.out.println("testB-" + Thread.currentThread().getName() + " sleep start");
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            System.out.println("testB-" + Thread.currentThread().getName() + " sleep end");
        }
    }

}
