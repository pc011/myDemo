package com.thread.demo6;

import java.util.concurrent.BlockingQueue;

/**
 * 支持关闭的多生产者-单消费者日志服务
 */
public class LogWritter2 {

    private final BlockingQueue<String> queue;
    private final LoggerThread logger;

    private boolean shutdownRequested = false;

    public LogWritter2(BlockingQueue<String> queue) {
        this.queue = queue;
        this.logger = new LoggerThread();
    }

    public void start() {
        logger.start();
    }

    public void log(String msg) throws InterruptedException {
        if (!shutdownRequested) {
            queue.put(msg);
        } else {
            throw new IllegalStateException("logger is shut down");
        }
    }

    public class LoggerThread extends Thread {

        @Override
        public void run() {
            try {
                while (true) {
                    System.out.println(queue.take());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


}
