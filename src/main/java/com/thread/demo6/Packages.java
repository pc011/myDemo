package com.thread.demo6;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class Packages {

    private final AtomicInteger num = new AtomicInteger(100);

    public Integer getPackage() {
        int i = num.getAndDecrement();
        if (i < 0) {
            //抢红包失败
            throw new RuntimeException();
        }
        return i;
    }

}
