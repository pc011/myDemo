package com.thread.chapter13.demo1;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 通过tryLock来避免锁顺序死锁
 * 使用tryLock获取两个锁，如果不能同时获得，则退回并重新尝试
 *
 * ReentrantLock与Synchronized都提供了相同的互斥性和可见性，在获取ReentrantLock锁时，与进入同步代码块的语义相同
 * 在释放锁时，与退出同步代码块语义相同。
 * Lock:无条件，可轮询，定时的以及可中断的锁获取操作。
 * Synchronized:内置锁无法中断一个正在等待获取锁的线程，无法在请求获取一个锁时无限等待下去，无法实现非阻塞结构的加锁规则。
 *
 * 什么时候使用ReentrantLock？可定时，可轮询，可中断的锁获取操作，公平队列，非块结构的锁时。
 */
public class LockTest {

    public boolean transferMoney(Account fromAccount, Account toAccount, BigDecimal amount, long timeout, TimeUnit unit) throws InterruptedException {
        long fixedDelay = TimeUnit.NANOSECONDS.toNanos(timeout); //不完善
        long randMod = (long)((double)fixedDelay * Math.random()); //不完善
        long stopTime = System.nanoTime() + unit.toNanos(timeout);

        while (true) {
            if (fromAccount.lock.tryLock()) {
                try {
                    if (toAccount.lock.tryLock()) {
                        try {
                            if (fromAccount.getBalance().compareTo(amount) < 0) {
                                throw new RuntimeException("余额不足");
                            } else {
                                fromAccount.debit(amount);
                                toAccount.credit(amount);
                                return true;
                            }
                        } finally {
                            toAccount.lock.unlock();
                        }
                    }
                } finally {
                    fromAccount.lock.unlock();
                }
            }
            //如果在指定时间内不能获得所需要的锁，则返回失败
            if (System.nanoTime() < stopTime) {
                return false;
            }
            //休眠时间不固定，降低发生活锁的可能性
            Thread.sleep(fixedDelay + randMod); //不完善
        }
    }

    //带有时间限制的加锁
    public boolean trySendOnSharedLine(String message, long timeout, TimeUnit unit) throws InterruptedException {
        long nanosToLock = unit.toNanos(timeout);
        Lock lock = new ReentrantLock();
        if (!lock.tryLock(nanosToLock, TimeUnit.NANOSECONDS)) {
            return false;
        }
        try {
            return sendOnSharedLine(message);
        } finally {
            lock.unlock();
        }
    }

    //可中断的锁获取操作，在获得锁的同时保持对中断的响应
    public boolean sendOnSharedLine(String message) throws InterruptedException {
        Lock lock = new ReentrantLock();
        lock.lockInterruptibly();
        try {
            cancellableSendOnSharedLine(message);
            return true;
        } finally {
            lock.unlock();
        }
    }

    public boolean cancellableSendOnSharedLine(String message) throws InterruptedException {
        return true;
    }

}
