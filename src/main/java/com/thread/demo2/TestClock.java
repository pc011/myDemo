package com.thread.demo2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicLong;

public class TestClock {

    private static final Long MAX_CLOCKS = 1000000L;

    private static volatile boolean end = false;

    private static AtomicLong clockN = new AtomicLong(1);


    public static void main(String[] args) throws InterruptedException {
        final BlockingQueue<Integer>[] queueArr = new LinkedBlockingDeque[60];
        for (int i = 0; i < queueArr.length; i++) {
            queueArr[i] = new LinkedBlockingDeque<>();
        }

        //int availableProcessors = Runtime.getRuntime().availableProcessors();
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        for (int k = 0; k < 100; k++) {
            executorService.submit(new ClockProducer(queueArr));
        }
        while (!end) {
            Thread.yield();
        }
        System.out.println("produce end");
        for (int m = 0; m < (MAX_CLOCKS/60); m++) {
            executorService.submit(new ClockCustomer(queueArr));
            Thread.currentThread().sleep(1000);
        }
    }

    private static class ClockProducer implements Runnable {

        private final BlockingQueue<Integer>[] queue;

        private ClockProducer(BlockingQueue<Integer>[] queue) {
            this.queue = queue;
        }

        @Override
        public void run() {
            while (queue[0].size() < MAX_CLOCKS) {
                try {
                    int second = randomSecond();
                    queue[second].put(second);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            end = true;
        }

        private int randomSecond() {
            return (int)(Math.random() * 60D);
        }

        private String randomClock() {
            Integer hour = (int)(Math.random() * 24D);
            Integer minute;
            Integer second;
            if (hour == 24) {
                minute = 0;
                second = 0;
            } else {
                minute = (int)(Math.random() * 60D);
                second = (int)(Math.random() * 60D);
            }
            String hours = hour < 10 ? ("0" + hour) : hour.toString();
            String minutes = minute < 10 ? ("0" + minute) : minute.toString();
            String seconds = second < 10 ? ("0" + second) : second.toString();
            return hours + ":" + minutes +":"+ seconds;
        }
    }

    private static class ClockCustomer implements Runnable {

        private final BlockingQueue<Integer>[] queue;

        private ClockCustomer(BlockingQueue<Integer>[] queue) {
            this.queue = queue;
        }

        @Override
        public void run() {
            try {
                int second = getCurrentSecond();
                while(true) {
                    int time = queue[second].take();
                    System.out.println(time);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private static int getCurrentSecond() {
            DateFormat dateFormat = new SimpleDateFormat("ss");
            return Integer.parseInt(dateFormat.format(new Date()));
        }

        private String getCurrentTimeStr() {
            DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
            return dateFormat.format(new Date());
        }

    }



}
