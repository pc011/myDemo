package com.thread.chapter05;

/**
 */
public class Board {

    private final Integer[][] range;
    private final int maxX;
    private final int maxY;

    public Board(int maxX, int maxY) {
        this.maxX = maxX;
        this.maxY = maxY;
        this.range = new Integer[maxX][maxY];
    }

    public int getMaxX() {
        return this.maxX;
    }

    public int getMaxY() {
        return this.maxY;
    }

    public int getValue(int x, int y) {
        return this.range[x][y];
    }

    public void setNewValue(int x, int y, int value) {
        this.range[x][y] = value;
    }

    public void commitNewValue() {
        //TODO
    }

    public boolean hasCoverage() {
        //TODO
        return false;
    }

    public void waitForCoverage() {
        //TODO
    }

    public Board getSubBoard(int numPartitions, int index) {
        //TODO
        return null;
    }

}
