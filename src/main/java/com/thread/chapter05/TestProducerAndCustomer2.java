package com.thread.chapter05;

import java.io.File;
import java.io.FileFilter;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicLong;

public class TestProducerAndCustomer2 {

    private static AtomicLong atomicLong = new AtomicLong(0);

    public static void main(String[] args) {
        System.out.println("start:" + new Date().getTime());
        File file = new File("D:/");
        File[] roots = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        });

        //startIndexing(roots); //1511766243679 - 1511766240129 3550
        startCrowlerAndIndexer(roots); //1511766315623 - 1511766311507 4116
    }

    private static void startCrowlerAndIndexer(File[] roots) {
        FileFilter fileFilter = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return true;
                //return pathname.isDirectory(); || pathname.getName().endsWith(".jar"); //7310
            }
        };
        for (int i = 0; i < roots.length; i ++) {
            new Thread(new FileCrowlerAndIndexer(fileFilter, roots[i])).start();
        }
    }

    public static void startIndexing(File[] roots) {
        BlockingQueue<File> blockingQueue = new LinkedBlockingDeque();
        FileFilter fileFilter = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return true;
                //return pathname.isDirectory();// || pathname.getName().endsWith(".jar"); //7310
            }
        };
        for (int i = 0; i < roots.length; i ++) {
            new Thread(new FileCrowler(blockingQueue, fileFilter, roots[i])).start();
        }
        for (int i = 0; i < 10; i ++) {
            new Thread(new FileIndexer(blockingQueue)).start();
        }
    }

    static class FileCrowlerAndIndexer implements Runnable {

        private final FileFilter fileFilter;

        private final File root;

        FileCrowlerAndIndexer(FileFilter fileFilter, File root) {
            this.fileFilter = fileFilter;
            this.root = root;
        }

        @Override
        public void run() {
            crowl(root);
        }

        private void crowl(File file) {
            File[] files = file.listFiles(fileFilter);
            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    File fileOrDictionary = files[i];
                    if (fileOrDictionary.isDirectory()) {
                        crowl(fileOrDictionary);
                    } else {
                        indexFile(file);
                    }
                }
            }
        }

        private void indexFile(File file) {
            System.out.println("file name:" + file.getAbsolutePath() + " num:" + atomicLong.incrementAndGet() + " date:" + new Date().getTime());
        }
    }

    static class FileCrowler implements Runnable {

        private final BlockingQueue<File> fileQueue;

        private final FileFilter fileFilter;

        private final File root;

        FileCrowler(BlockingQueue<File> fileQueue, FileFilter fileFilter, File root) {
            this.fileQueue = fileQueue;
            this.fileFilter = fileFilter;
            this.root = root;
        }

        @Override
        public void run() {
            crowl(root);
        }

        private void crowl(File file) {
            File[] files = file.listFiles(fileFilter);
            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    File fileOrDictionary = files[i];
                    if (fileOrDictionary.isDirectory()) {
                        crowl(fileOrDictionary);
                    } else if (!alreadyIndexed(fileOrDictionary)) {
                        fileQueue.add(fileOrDictionary);
                    }
                }
            }
        }

        private boolean alreadyIndexed(File fileOrDictionary) {
            return fileQueue.contains(fileOrDictionary);
        }

    }

    static class FileIndexer implements Runnable {

        private final BlockingQueue<File> blockingQueue;

        FileIndexer(BlockingQueue<File> blockingQueue) {
            this.blockingQueue = blockingQueue;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    File file = blockingQueue.take();
                    indexFile(file);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }

        private void indexFile(File file) {
            System.out.println("file name:" + file.getAbsolutePath() + " num:" + atomicLong.incrementAndGet() + " date:" + new Date().getTime());
        }

    }

}
