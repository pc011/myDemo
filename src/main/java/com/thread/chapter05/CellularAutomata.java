package com.thread.chapter05;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * 栅栏（Barrier）测试：通过CyclicBarrier协调细胞自动衍生系统中的计算
 * 栅栏类似于闭锁，他能阻塞一组线程直到某个事件发生。
 * 栅栏与闭锁的关键区别在于：所有线程必须同时到达栅栏位置，才能继续执行。
 * 闭锁用于等待事件，栅栏用于等待其他线程。
 */
public class CellularAutomata {

    private final Board mainBoard;

    private final CyclicBarrier cyclicBarrier;

    private final Worker[] workers;


    public CellularAutomata(Board board) {
        this.mainBoard = board;
        int count = Runtime.getRuntime().availableProcessors();
        cyclicBarrier = new CyclicBarrier(count, new Runnable() {
            public void run() {
                mainBoard.commitNewValue();
            }
        });
        this.workers = new Worker[count];
        for (int i = 0; i < count; i++) {
            workers[i] = new Worker(mainBoard.getSubBoard(count, i));
        }
    }

    private class Worker implements Runnable {

        private final Board board;

        public Worker(Board board) {
            this.board = board;
        }

        public void run() {
            while (!board.hasCoverage()) {
                for (int x = 0; x < board.getMaxX(); x++) {
                    for (int y = 0; y < board.getMaxY(); y++) {
                        board.setNewValue(x, y, computeValue(x, y));
                    }
                }
                try {
                    //等待直到所有线程都到达这个位置
                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }

        private int computeValue(int x, int y) {
            return (int)(Math.random() * 100);
        }
    }

    public void start() {
        for(int i = 0; i < workers.length; i++) {
            new Thread(workers[i]).start();
        }
        mainBoard.waitForCoverage();
    }

}
