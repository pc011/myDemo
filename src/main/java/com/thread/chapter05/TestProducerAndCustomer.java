package com.thread.chapter05;

import java.io.File;
import java.io.FileFilter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 桌面搜索应用程序中的生产者任务和消费者任务
 * 通过阻塞队列实现生产者与消费者模式
 */
public class TestProducerAndCustomer {

    private static AtomicLong atomicLong = new AtomicLong(0);

    public static void main(String[] args) {
        File file = new File("D:/");
        File[] roots = file.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        });

        startIndexing(roots);
    }

    public static void startIndexing(File[] roots) {
        BlockingQueue<File> queue = new LinkedBlockingQueue<File>(100000);
        for (File root: roots) {
            //文件过滤器
            FileFilter fileFilter = new FileFilter() {
                public boolean accept(File pathname) {
                    return true;
                }
            };
            new Thread(new FileCrawler(queue, fileFilter, root)).start();
        }

        for (int i=0;i<100;i++) {
            new Thread(new Indexer(queue)).start();
        }

    }

    /***
     * 将文件读入队列
     */
    static class FileCrawler implements Runnable {
        //缓存文件的队列
        private final BlockingQueue<File> fileQueue;
        //文件过滤器
        private final FileFilter fileFilter;
        //搜索跟目录
        private final File root;

        public FileCrawler(BlockingQueue<File> fileQueue, FileFilter fileFilter, File root) {
            this.fileQueue = fileQueue;
            this.fileFilter = fileFilter;
            this.root = root;
        }

        public void run() {
            try {
                crawl(root);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        //读取当前目录下的文件并存入队列
        private void crawl(File root) throws InterruptedException {
            File[] entries = root.listFiles(fileFilter);
            if (entries != null) {
                for (File file : entries) {
                    if (file.isDirectory()) {
                        crawl(file);
                    } else if (!alreadyIndexed(file)) {
                        fileQueue.put(file);
                    }
                }
            }
        }

        private boolean alreadyIndexed(File file) {
            return fileQueue.contains(file);
        }
    }

    /**
     * 将文件从队列取出
     */
    static class Indexer implements Runnable {

        private final BlockingQueue<File> queue;

        public Indexer(BlockingQueue<File> queue) {
            this.queue = queue;
        }

        public void run() {
            while (true) {
                try {
                    indexFile(queue.take());
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }

        private void indexFile(File file) {

            System.out.println("indexing file " + file.getAbsolutePath() + " num:" + atomicLong.incrementAndGet());
        }
    }

}

