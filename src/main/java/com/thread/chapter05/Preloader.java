package com.thread.chapter05;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 使用FutureTask来提前加载稍后需要的数据
 */
public class Preloader {

    //执行运算的线程
    private final FutureTask<ProductInfo> futureTask = new FutureTask<ProductInfo>(new Callable<ProductInfo>() {
        public ProductInfo call() throws Exception {
            return loadProductInfo();
        }
    });

    private final Thread thread = new Thread(futureTask);

    //启动线程的方法
    public void start() {
        thread.start();
    }

    //返回数据，如果未加载完成则等待加载完成后返回
    public ProductInfo get() throws InterruptedException, ExecutionException {
        try {
            return futureTask.get();
        } catch (InterruptedException e) {
            throw e;
        } catch (ExecutionException e) {
            throw e;
        }
    }

    //加载产品信息
    private ProductInfo loadProductInfo() {
        return null;
    }

    static class ProductInfo{

    }

}
