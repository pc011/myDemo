package com.thread.chapter05;

import java.util.concurrent.CountDownLatch;

/**
 * 在计时测试中使用CountDownLatch来启动和停止线程
 * 闭锁（CountDownLatch）测试
 * 闭锁：闭锁是一种同步工具类，可以延迟线程的进度直到其到达终止状态。
 * 闭锁相当于一扇门，在闭锁到达结束状态之前，这扇门一直是关闭的，并且没有任何线程能够通过，
 * 当到达结束状态时，这扇门会打开并允许所有的线程通过，并且结束后不会再改变状态，这扇门的将永远保持打开状态。
 */
public class TestHarness {

    public static void main(String[] args) {
        try {
            long time = timeTasks(3, new Runnable() {
                public void run() {
                    try {
                        System.out.println(Thread.currentThread().getName() + " start");
                        Thread.sleep(1000);
                        System.out.println(Thread.currentThread().getName() + " end");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            System.out.println("执行时长:" + time);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    /**
     * 二元闭锁实现
     * @param nThreads 工作线程数量
     * @param task 工作任务
     * @return 工作任务执行时长
     * @throws InterruptedException
     */
    public static long timeTasks(int nThreads, final Runnable task) throws InterruptedException {
        //startGate确保所有任务都就绪之后再开始执行
        final CountDownLatch startGate = new CountDownLatch(1);
        //endGate确保所有任务都已执行完毕
        final CountDownLatch endGate = new CountDownLatch(nThreads);

        for (int i = 0; i < nThreads; i++) {
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    try {
                        startGate.await(); //使所有工作线程等待
                        try {
                            //执行任务
                            task.run();
                        } finally {
                            //每执行完一个任务,endGate.countDown-1;
                            endGate.countDown();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        }

        long start = System.currentTimeMillis();
        //所有工作任务开始运行(startGate.countDown==0)
        startGate.countDown();
        //主线程开始等待，直到所有任务执行完毕(endGate.countDown==0)
        endGate.await();
        long end = System.currentTimeMillis();
        return end - start;
    }


}
