package com.thread.chapter05.demo3;

import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Memoizer的最终实现，使用putIfAbsent，避免了Memoizer3中可能出现的非原子性的问题
 *
 */
public class Memoizer4 implements Computable<String, BigInteger> {

    private final Map<String, Future<BigInteger>> cache = new ConcurrentHashMap<String, Future<BigInteger>>();
    private final Computable<String, BigInteger> c;

    public Memoizer4(Computable<String, BigInteger> c) {
        this.c = c;
    }

    public BigInteger compute(final String arg) throws InterruptedException {
        Future<BigInteger> future = cache.get(arg);
        if (future == null) {
            Callable<BigInteger> eval = new Callable<BigInteger>() {
                public BigInteger call() throws Exception {
                    return c.compute(arg);
                }
            };
            FutureTask<BigInteger> futureTask = new FutureTask<BigInteger>(eval);
            //使用原子方法避免来确保复合操作“若没有则添加的原子性”
            future = cache.putIfAbsent(arg, futureTask);
            if (future == null) {
                future = futureTask;
                futureTask.run();
            }
        }
        try {
            return future.get();
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

}
