package com.thread.chapter05.demo3;

import java.math.BigInteger;

/**
 */
public class ExpensiveFunction implements Computable<String, BigInteger> {

    //表示需要很长时间来计算结果
    public BigInteger compute(String arg) throws InterruptedException {
        Thread.sleep(1000);
        return new BigInteger(arg);
    }

}
