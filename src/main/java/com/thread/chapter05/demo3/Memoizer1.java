package com.thread.chapter05.demo3;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * synchronized 线程同步，可伸缩性差，线程阻塞时间长
 */
public class Memoizer1 implements Computable<String, BigInteger> {

    //使用hashMap来保存之前计算的结果
    private final Map<String, BigInteger> cache = new HashMap<String, BigInteger>();
    private final Computable<String, BigInteger> c;

    public Memoizer1(Computable<String, BigInteger> c) {
        this.c = c;
    }

    //对整个compute方法进行同步，保证线程安全性
    public synchronized BigInteger compute(String arg) throws InterruptedException {
        BigInteger result = cache.get(arg);
        if (result == null) {
            result = c.compute(arg);
            cache.put(arg, result);
        }
        return result;
    }
}
