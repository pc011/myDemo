package com.thread.chapter05.demo3;

import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.*;

/**
 * 使用FutureTask，降低Memoizer2中出现重复计算的概率
 * 优点：并发性好，如果其它线程正在计算这个结果，那么新的线程会等待直到这个结果被计算出来
 * 漏洞：任然存在重复计算的漏洞
 */
public class Memoizer3 implements Computable<String, BigInteger> {

    private final Map<String, Future<BigInteger>> cache = new ConcurrentHashMap<String, Future<BigInteger>>();
    private final Computable<String, BigInteger> c;

    public Memoizer3(Computable<String, BigInteger> c) {
        this.c = c;
    }

    public BigInteger compute(final String arg) throws InterruptedException {
        Future<BigInteger> future = cache.get(arg);
        //首先检查某一个计算是否已经开始
        if (future == null) {
            //如果还没开始则创建一个futureTask
            Callable<BigInteger> eval = new Callable<BigInteger>() {
                public BigInteger call() throws Exception {
                    return c.compute(arg);
                }
            };
            FutureTask<BigInteger> futureTask = new FutureTask<BigInteger>(eval);
            future = futureTask;
            cache.put(arg, futureTask);
            //启动计算
            futureTask.run();
        }
        try {
            //等待现有计算的结果
            return future.get();
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

}
