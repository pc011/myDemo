package com.thread.chapter05.demo3;

import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * ConcurrentHashMap 并发，线程安全，但可能会同一个值进行重复计算
 */
public class Memoizer2 implements Computable<String, BigInteger> {

    //使用ConcurrentHashMap来保存之前计算的结果
    private final Map<String, BigInteger> cache = new ConcurrentHashMap<String, BigInteger>();
    private final Computable<String, BigInteger> c;

    public Memoizer2(Computable<String, BigInteger> c) {
        this.c = c;
    }

    public BigInteger compute(String arg) throws InterruptedException {
        BigInteger result = cache.get(arg);
        //首先判断某一个计算的结果是否已经完成
        if (result == null) {
            result = c.compute(arg);
            cache.put(arg, result);
        }
        return result;
    }

}
