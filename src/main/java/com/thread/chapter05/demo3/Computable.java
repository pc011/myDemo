package com.thread.chapter05.demo3;

/**
 * 构建高效可伸缩的结果缓存
 * A 输入类型
 * V 输出类型
 */
public interface Computable<A, V> {

    V compute(A arg) throws InterruptedException;

}
