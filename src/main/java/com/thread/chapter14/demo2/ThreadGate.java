package com.thread.chapter14.demo2;

/**
 * 使用wait()和notify()实现可重新关闭的阀门
 */
public class ThreadGate {

    private boolean isOpen;
    private int generation = 0;

    public synchronized void close() {
        isOpen = false;
    }

    public synchronized void open() {
        ++generation;
        isOpen = true;
        notifyAll();
    }

    public synchronized void await() throws InterruptedException {
        int arrivalGeneration = generation;
        while (!isOpen && arrivalGeneration == generation) {
            wait();
        }
    }

}
