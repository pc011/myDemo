package com.thread.chapter14.demo3;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 使用显示条件变量的有界缓存
 */
public class ConditionBoundedBuffer<T> {

    private static final int BUFFER_SIZE = 10;
    protected final Lock lock = new ReentrantLock();
    private final Condition notFull = lock.newCondition();
    private final Condition notEmpty = lock.newCondition();
    private final T[] items = (T[])new Object[BUFFER_SIZE];
    private int tail = 0,head = 0,count = 0;

    //阻塞直到not full
    public void put(T t) throws InterruptedException {
        lock.lock();
        try {
            while (count == items.length) {
                notFull.wait();
            }
            items[tail++] = t;
            if (tail == items.length) {
                tail = 0;
            }
            ++count;
            notEmpty.signal();
        } finally {
            lock.unlock();
        }
    }

    //阻塞直到not empty
    public T take() throws InterruptedException {
        lock.lock();
        try {
            while (count == 0) {
                notEmpty.wait();
            }
            T t = items[  head++];
            if (head == items.length) {
                head = 0;
            }
            notFull.signal();
            return t;
        } finally {
            lock.unlock();
        }
    }

}
