package com.thread.chapter14.demo5;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * 使用AbstractQueuedLongSynchronizer实现的二元闭锁
 * AQS状态用来表示闭锁状态，关闭（0），打开（1），
 *
 * AQS是一个用于构建锁和同步器的框架。
 * 它解决了在实现同步器时涉及的大量细节问题。
 * 在基于AQS构建的同步器类中，最基本的操作包括各种形式的 获取 和 释放 操作。
 * AQS负责管理同步器类中的状态，它管理了一个整数状态信息，可以通过getState和setState等方法进行操作。
 *
 */
public class OneShotLatch {

    private final Sync sync = new Sync();

    public void signal() {
        sync.releaseShared(0);
    }

    public void await() throws InterruptedException {
        sync.acquireSharedInterruptibly(0);
    }

    private class Sync extends AbstractQueuedSynchronizer {
        @Override
        protected int tryAcquireShared(int ignored) {
            //如果锁是开的，那么这个操作将成功，否则将失败
            return getState() == 1 ? 1 : -1;
        }

        @Override
        protected boolean tryReleaseShared(int ignored) {
            setState(1);
            return true;
        }
    }

}
