package com.thread.chapter14.demo1;

/**
 * 有界缓存实现3
 * 使用条件队列实现的有界缓存
 */
public class BoundedBuffer<V> extends BaseBoundedBuffer<V> {

    public BoundedBuffer(int capacity) {
        super(capacity);
    }

    //阻塞直到not full
    public synchronized void put(V v) throws InterruptedException {
        while (isFull()) {
            wait();
        }
        boolean wasEmpty = isEmpty();//使用条件通知
        doPut(v);
        if (wasEmpty) {
            notifyAll();
        }
    }

    //阻塞直到not empty
    public synchronized V take() throws InterruptedException {
        while (isEmpty()) {
            wait();
        }
        boolean wasFull = isFull();//使用条件通知
        V v = doTake();
        if (wasFull) {
            notifyAll();
        }
        return v;
    }

}
