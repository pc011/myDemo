package com.thread.chapter14.demo1;

/**
 * 有界缓存实现的基类
 */
public abstract class BaseBoundedBuffer<V> {

    private final V[] buf;
    private int tail;
    private int head;
    private int count;

    protected BaseBoundedBuffer(int capacity) {
        buf = (V[])new Object[capacity];
        count = tail = head = 0;
    }

    protected synchronized final void doPut(V v) {
        buf[tail++] = v;
        if (tail == buf.length) {
            tail = 0;
        }
        ++count;
    }

    protected synchronized final V doTake() {
        V v = buf[head];
        buf[head] = null;
        if (++head == buf.length) {
            head = 0;
        }
        -- count;
        return v;
    }

    protected synchronized final boolean isFull() {
        return count == buf.length;
    }

    protected synchronized final boolean isEmpty() {
        return count == 0;
    }

}
