package com.thread.chapter14.demo1;

/**
 * 有界缓存实现1
 * 先检查再运行，当不满足前提条件时，有界缓存不会实现相应的操作
 */
public class GrumpyBoundedBuffer<V> extends BaseBoundedBuffer<V> {

    public GrumpyBoundedBuffer(int capacity) {
        super(capacity);
    }

    public synchronized void put(V v) throws BufferFullException {
        if (isFull()) {
            throw new BufferFullException();
        }
        doPut(v);
    }

    public synchronized V take() throws BufferFullException {
        if (isEmpty()) {
            throw new BufferEmptyException();
        }
        return doTake();
    }

}
