package com.thread.chapter14.demo1;

/**
 * 有界缓存实现2
 * 使用简单阻塞来实现有界缓存
 * 通过轮询与休眠来实现
 */
public class SleepyBoundedBuffer<V> extends BaseBoundedBuffer<V> {

    private static final Long SLEEP_GRANULARITY = 100L;

    public SleepyBoundedBuffer(int capacity) {
        super(capacity);
    }

    public void put(V v) throws InterruptedException {
        while (true) {
            synchronized (this) {
                if (!isFull()) {
                    doPut(v);
                    return;
                }
            }
            Thread.sleep(SLEEP_GRANULARITY);
        }
    }

    public V take(V v) throws InterruptedException {
        while (true) {
            synchronized (this) {
                if (!isEmpty()) {
                    return doTake();
                }
            }
            Thread.sleep(SLEEP_GRANULARITY);
        }
    }

}
