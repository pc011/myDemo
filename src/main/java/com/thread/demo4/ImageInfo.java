package com.thread.demo4;

/**
 * 代表图片链接等信息
 */
public class ImageInfo {

    /**
     * 下载图片
     * @return
     */
    public ImageData downloadImage() {
        System.out.println("下载图片....");
        return new ImageData();
    }
}
