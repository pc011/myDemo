package com.thread.demo4;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * 使用Future等待图片下载
 * 下载所有图片后一起进行渲染
 */
public class FutureRender extends Render {

    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    public void renderPage(CharSequence source) {
        final List<ImageInfo> imageInfoList = scanForImageInfo(source);
        Callable<List<ImageData>> task = new Callable<List<ImageData>>() {
            @Override
            public List<ImageData> call() throws Exception {
                List<ImageData> result = new ArrayList<>();
                for (ImageInfo imageInfo : imageInfoList) {
                    result.add(imageInfo.downloadImage());
                }
                return result;
            }
        };
        Future<List<ImageData>> future = executorService.submit(task);
        renderText(source);
        try {
            List<ImageData> imageDataList = future.get();
            for (ImageData imageData: imageDataList) {
                renderImage(imageData);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
            future.cancel(true);
        } catch (ExecutionException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }
    }

}
