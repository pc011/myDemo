package com.thread.demo4;

import java.util.List;
import java.util.concurrent.*;

/**
 * 使用CompletionService,使图片被下载后立即显示
 */
public class Renderer extends Render {

    private static long TIME_BUDGE = 1000;

    private static Ad DEFAULT_AD = new Ad();

    private final ExecutorService executor;

    public Renderer(ExecutorService executor) {
        this.executor = executor;
    }

    public void renderPage(CharSequence source) {
        List<ImageInfo> imageInfoList = scanForImageInfo(source);
        CompletionService<ImageData> completionService = new ExecutorCompletionService<ImageData>(executor);
        for (ImageInfo imageInfo : imageInfoList) {
            completionService.submit(new Callable<ImageData>() {
                @Override
                public ImageData call() throws Exception {
                    return imageInfo.downloadImage();
                }
            });
        }
        renderText(source);

        for (int i = 0,num = imageInfoList.size(); i < num; i++) {
            try {
                Future<ImageData> future = completionService.take();
                ImageData imageData = future.get();
                renderImage(imageData);
            } catch (InterruptedException e) {
                e.printStackTrace();
                Thread.currentThread().interrupt();
            } catch (ExecutionException e) {
                e.printStackTrace();
                throw new RuntimeException(e.getCause());
            }
        }

    }

    public void renderPageWithAd(CharSequence source) {
        long endNaos = System.nanoTime() + TIME_BUDGE;
        Future<Ad> future = executor.submit(new Callable<Ad>() {
            @Override
            public Ad call() throws Exception {
                return new Ad();
            }
        });

        renderPage(source);

        Ad ad;
        long timeLeft = endNaos - System.nanoTime();
        try {
            ad = future.get(timeLeft, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            ad = DEFAULT_AD;
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            e.printStackTrace();
            ad = DEFAULT_AD;
            throw new RuntimeException(e.getCause());
        } catch (TimeoutException e) {
            ad = DEFAULT_AD;
            future.cancel(true);
            e.printStackTrace();
        }

        renderAd(ad);

    }


}