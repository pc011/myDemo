package com.thread.demo4;

import java.util.List;

public class Render {

    /**
     * 渲染图片
     * @param imageData
     */
    public void renderImage(ImageData imageData) {
        System.out.println("渲染图片....");
    }

    /**
     * 渲染文字
     * @param source
     */
    public void renderText(CharSequence source) {
        System.out.println("渲染文字....");
    }

    /**
     * 渲染广告
     */
    public void renderAd(Ad ad) {
        System.out.println("渲染广告....");
    }

    /**
     * 扫描图片
     * @param sequence
     * @return
     */
    public List<ImageInfo> scanForImageInfo(CharSequence sequence) {
        return null;
    }

    static class Ad {

    }

}
