package com.thread.demo1;

public interface ArchiveSearcher {

    String search(String target);

}
