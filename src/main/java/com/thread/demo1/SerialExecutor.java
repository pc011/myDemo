package com.thread.demo1;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Executor;

/**
 * 演示复合执行器，将任务的提交序列化到第二个执行者
 */
public class SerialExecutor implements Executor {

    final Queue<Runnable> tasks = new ArrayDeque<Runnable>();
    final Executor executor;
    Runnable task;

    public SerialExecutor(Executor executor) {
        this.executor = executor;
    }

    public synchronized void execute(final Runnable command) {
        tasks.offer(new Runnable() {
            @Override
            public void run() {
                try {
                    command.run();
                } finally {
                    executeNext();
                }
            }
        });
        if (task == null) {
            executeNext();
        }
    }

    public synchronized void executeNext() {
        if ((task = tasks.poll()) != null) {
            executor.execute(task);
        }
    }

    public static void main(String[] args) {
        SerialExecutor executor = new SerialExecutor(new MyExecutor());
        executor.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("thread end");
            }
        });
        System.out.println("main end");
    }
}
