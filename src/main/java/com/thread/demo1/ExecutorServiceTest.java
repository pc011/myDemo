package com.thread.demo1;

import java.util.*;
import java.util.ArrayList;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 */
public class ExecutorServiceTest {
    //单一工作线程，无限队列，FIFO，LIFO
    //private static ExecutorService executorService = Executors.newSingleThreadExecutor();
    //可容纳3个任务的线程池，无限队列，定常线程池
    //private static ExecutorService executorService = Executors.newFixedThreadPool(3);
    //线程池为无限大,如果之前的线程已回收，则会复用，否则会新建。
    //适合大量的生命周期短的任务。
    //private static ExecutorService executorService = Executors.newCachedThreadPool();
    //并发等级为3（最大生效线程数量）
    private static ScheduledExecutorService executorService = Executors.newScheduledThreadPool(10);
//    //并发等级为3（最大生效线程数量）
//    private static ExecutorService executorService = Executors.newWorkStealingPool(10);

    private static AtomicInteger num = new AtomicInteger(0);

    public static void main(String[] args) {
        //testExecute();

        //testInvoke();

        //testSubmit();

        //testIncrease();

        testInvoceDelay();
    }

    public static void testInvoceDelay() {
        Future future = executorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                int i = num.getAndIncrement();
                System.out.println(Thread.currentThread().getName() + " task "+ i +" start");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " task "+ i + " end");

            }
        }, 1, 1, TimeUnit.SECONDS);
        try {
            System.out.println(Thread.currentThread().getName() + future.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public static void testSubmit() {
        Future future = executorService.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + " start ---");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " end ---");
            }
        }, "111");
        try {
            System.out.println(Thread.currentThread().getName() + future.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public static void testIncrease() {

        List<Callable<String>> callableList = new ArrayList<Callable<String>>();
        for (int i = 0; i < 1000; i++) {
            Callable callable = new Callable() {
                @Override
                public Object call() throws Exception {
                    for (int j=0;j<10;j++) {
                        //num++;
                    }
                    return null;
                }
            };
            callableList.add(callable);
        }
        try {
            List<Future<String>> futures = executorService.invokeAll(callableList);
            System.out.println(Thread.currentThread().getName() + " num:" + num);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " num:" + num);
    }

    public static void testInvoke() {
        List<Callable<String>> callableList = new ArrayList<Callable<String>>();
        for (int i = 0; i < 20; i++) {
            final int j = i;
            Callable callable = new Callable() {
                @Override
                public Object call() throws Exception {
                    System.out.println(Thread.currentThread().getName() + " start ---" +j);
                    Thread.sleep(1000);
                    System.out.println(Thread.currentThread().getName() + " end ---" +j);
                    return j+"";
                }
            };
            callableList.add(callable);
        }
        try {
            List<Future<String>> futures = executorService.invokeAll(callableList);
            for (Future future: futures) {
                String str = (String) future.get();
                System.out.println(Thread.currentThread().getName() + "---future " +str);
            }
//            String str = executorService.invokeAny(callableList, 1, TimeUnit.SECONDS);
//            System.out.println(Thread.currentThread().getName() + "---future " +str);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
//        catch (TimeoutException e) {
//            e.printStackTrace();
//        }
        System.out.println(Thread.currentThread().getName());
    }

    public static void testExecute() {
        Set<String> set = new HashSet<String>();
        for (int i = 0; i < 1000; i++) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName() + " start ---");
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                    System.out.println(Thread.currentThread().getName() + " end ---");
                    //set.add(Thread.currentThread().getName());
                }
            });
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("set size is : " + set.size());
    }

}
