package com.thread.demo1;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class NetworkService implements Runnable {

    private final ServerSocket serverSocket;
    private final ExecutorService pool;

    public NetworkService(int port, int poolSize) throws IOException {
        this.serverSocket = new ServerSocket(port);
        this.pool = Executors.newFixedThreadPool(poolSize);
    }

    @Override
    public void run() {
        try {
            for (;;){
                pool.execute(new Handler(serverSocket.accept()));
            }
        } catch (IOException e) {
            pool.shutdown();
        }
    }

    void shutdownAndAwaitTermination(ExecutorService pool) {
        pool.shutdown(); //禁止新任务提交
        try {
            //等待已存在的任务终止
            if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
                pool.shutdownNow();//取消当前执行的任务
                //等待任务响应取消
                if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
                    System.err.println("Pool did not terminate");
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            //如果当前线程也被打断则再次取消
            pool.shutdownNow();
            //保存打断状态
            Thread.currentThread().interrupt();
        }
    }

}

class Handler implements Runnable {

    private final Socket socket;

    public Handler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        // read and service request on socket
    }
}