package com.thread.demo1;

import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ExecutorTest {

    public static void main(String[] args) {

        //异步执行
//        Executor executor = new ThreadPoolExecutor(1, 1,
//                0L, TimeUnit.MILLISECONDS,
//                new LinkedBlockingQueue<Runnable>());
//        executor.execute(new Runnable() {
//            public void run() {
//                System.out.println("任务1");
//            }
//        });
//        executor.execute(new Runnable() {
//            public void run() {
//                System.out.println("任务2");
//            }
//        });
//        System.out.println("main end");

        MyExecutor executor1 = new MyExecutor();
        executor1.execute(new Runnable() {
            public void run() {
                System.out.println("任务1");
            }
        });
        System.out.println("main end");


    }
}
