package com.thread.demo1;

import java.util.concurrent.Executor;

/**
 */
public class MyExecutor implements Executor {

    public void execute(Runnable command) {
        System.out.println("MyExecutor start");
        //command.run();//同步
        new Thread(command).start();//异步
        System.out.println("MyExecutor end");
    }

}
