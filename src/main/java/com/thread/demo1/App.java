package com.thread.demo1;

import java.util.concurrent.*;

/**
 */
public class App {

    final ExecutorService executor;
    final ArchiveSearcher searcher;

    public App() {
        executor = Executors.newSingleThreadExecutor();
        searcher = new ArchiveSearcherImpl();
    }

    void showTarget(final String target) {
        Future<String> future = executor.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println(Thread.currentThread().getName() + "线程1开始执行");
                ExecutorService executor1 = Executors.newSingleThreadExecutor();
                Future<String> future1 = executor1.submit(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        System.out.println(Thread.currentThread().getName() + "线程2开始执行");
                        Thread.sleep(5000);
                        return Thread.currentThread().getName() + "线程2执行完毕";
                    }
                });
                try {
                    System.out.println(future1.get());
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return Thread.currentThread().getName() + "线程1执行完毕";
            }
        });

        try {
            System.out.println(future.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        App app = new App();
        app.showTarget("1111");
        System.out.println("finish");
    }


}
