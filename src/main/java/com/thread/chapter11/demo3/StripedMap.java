package com.thread.chapter11.demo3;

import java.util.Objects;

/**
 * 在基于散列的Map中使用锁分段技术
 * 对一组独立对象上的锁进行分解
 */
public class StripedMap<K, V> {
    //同步策略:buckets[n]由locks[n%N_LOCKS]来保护
    private static final int N_LOCKS = 16;
    private final Node<K, V>[] buckets;
    private final Object[] locks;

    private static class Node<K, V> {
        final int hash;
        final K key;
        V value;
        Node<K, V> next;

        Node(int hash, K key, V value, Node<K, V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }

        private final K getKey() {
            return key;
        }

        private final V getValue() {
            return value;
        }

        @Override
        public final int hashCode() {
            return Objects.hashCode(key) ^ Objects.hashCode(value);
        }
    }

    public StripedMap(int numBuckets) {
        buckets = new Node[numBuckets];
        locks = new Object[N_LOCKS];
        for (int i = 0; i < N_LOCKS; i++) {
            locks[i] = new Object();
        }
    }

    private final int hash(K key) {
        return Math.abs(key.hashCode() % buckets.length);
    }

    private Object get(K key) {
        int hash = hash(key);
        synchronized (locks[hash%N_LOCKS]) {
            for (Node node = buckets[hash]; node != null; node = node.next) {
                if (node.key.equals(key)) {
                    return get(key);
                }
            }
        }
        return null;
    }

}
