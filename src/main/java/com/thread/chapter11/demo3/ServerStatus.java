package com.thread.chapter11.demo3;

import java.util.HashSet;
import java.util.Set;

/**
 * 对锁进行分解
 */
public class ServerStatus {

    private final Set<String> users;
    private final Set<String> queries;

    public ServerStatus() {
        this.users = new HashSet<>();
        this.queries = new HashSet<>();
    }

    public synchronized void addUser(String user) {
        users.add(user);
    }

    public synchronized void addQuery(String query) {
        queries.add(query);
    }

    public synchronized void removeUser(String user) {
        users.remove(user);
    }

    public synchronized void removeQuery(String query) {
        queries.remove(query);
    }

}
