package com.thread.chapter11.demo3;

import java.util.HashSet;
import java.util.Set;

/**
 * 将ServerStatus改写为使用锁分解技术
 * 每个状态通过单独的锁来进行保护
 */
public class ServerStatus2 {

    private final Set<String> users;
    private final Set<String> queries;

    public ServerStatus2() {
        this.users = new HashSet<>();
        this.queries = new HashSet<>();
    }

    public void addUser(String user) {
        synchronized (users) {
            users.add(user);
        }
    }

    public void addQuery(String query) {
        synchronized (queries) {
            queries.add(query);
        }
    }

    public void removeUser(String user) {
        synchronized (users) {
            users.remove(user);
        }
    }

    public void removeQuery(String query) {
        synchronized (queries) {
            queries.remove(query);
        }
    }

}

