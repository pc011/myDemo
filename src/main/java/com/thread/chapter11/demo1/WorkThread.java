package com.thread.chapter11.demo1;

import java.util.concurrent.BlockingQueue;

/**
 * 对任务队列的串行访问
 */
public class WorkThread extends Thread {

    private final BlockingQueue<Runnable> queue;

    public WorkThread(BlockingQueue<Runnable> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Runnable task = queue.take();
                task.run();
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
