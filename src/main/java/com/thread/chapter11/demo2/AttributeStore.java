package com.thread.chapter11.demo2;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class AttributeStore {

    private final Map<String, String> map = new HashMap<>();

    /**
     * 将一个锁不必要的持有过长时间
     * @param name
     * @param regexp
     * @return
     */
    public synchronized boolean userLocationMatched1(String name, String regexp) {
        String key = "users." + name + ".location";
        String location = map.get(key);
        return location != null && Pattern.matches(regexp, location);
    }

    /**
     * 减少锁持有的时间
     * @param name
     * @param regexp
     * @return
     */
    public boolean userLocationMatched2(String name, String regexp) {
        String key = "users." + name + ".location";
        String location = null;
        synchronized (this) {
            location = map.get(key);
        }
        return location != null && Pattern.matches(regexp, location);
    }

}
