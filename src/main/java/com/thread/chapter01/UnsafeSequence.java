package com.thread.chapter01;

/**
 * 非线程安全的序列生成器
 */
public class UnsafeSequence {

    private int sequence = 0;

    public int getNext() {
        return sequence++;
    }

}
