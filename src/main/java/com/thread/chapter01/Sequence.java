package com.thread.chapter01;

/**
 * 线程安全的序列生成器
 */
public class Sequence {

    private int sequence = 0;

    public synchronized int getNext() {
        return sequence++;
    }
}
