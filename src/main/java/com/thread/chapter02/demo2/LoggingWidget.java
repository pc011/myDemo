package com.thread.chapter02.demo2;

/**
 * 内置锁可重入，防止以下代码发生死锁
 * 重入：如果某个线程试图获取一个由它自己持有的锁，那么这个请求会成功。
 */

public class LoggingWidget extends Widget {

    @Override
    public synchronized void doSomething() {
        System.out.println("call do something");
        super.doSomething();
    }
}

class Widget {

    public synchronized void doSomething() {

    }

}