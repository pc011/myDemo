线程安全性-主要概念如下：
原子性
    竞态条件:当某一计算结果的正确性取决于多个线程的交替执行时序时，就会发生竞态条件。LazyInitRace.java
    复合操作:先检查后执行，读取-修改-写入等 CachingFactorizer.java
加锁机制
    内置锁：synchronized
    重入：如果某个线程试图获取一个由它自己持有的锁，那么这个请求会成功。LoggingWidget.java
用锁来保护状态，活跃性与性能 CachingFactorizer.java