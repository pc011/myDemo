package com.thread.chapter02.demo1;

/**
 * 延迟初始化-存在竞态条件
 * 竞态条件：
 *      当某一计算结果的正确性取决于多个线程的交替执行时序时，就会发生竞态条件。
 */
public class LazyInitRace {

    private ExpensiveObject instance = null;

    public ExpensiveObject getInstance() {
        if (instance == null) {
            instance = new ExpensiveObject();
        }
        return instance;
    }

    static class ExpensiveObject {

    }

}
