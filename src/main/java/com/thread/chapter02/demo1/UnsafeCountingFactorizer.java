package com.thread.chapter02.demo1;

import com.thread.chapter02.demo1.MyAbstractServlet;

import javax.servlet.*;
import java.math.BigInteger;

/**
 * 在没有同步的情况下统计已处理请求数量的Servlet，有状态，非线程安全
 * 线程安全性：
 *      当多个线程访问同一个类时，不管运行时环境采取何种调度方式或者这些线程将如何交替执行，
 *      并且在主调代码中不需要加任何额外的同步或协同，这个类都能表现出正确的行为，那么久称这个类是线程安全的。
 */
public class UnsafeCountingFactorizer extends MyAbstractServlet {

    private long count = 0;

    public long getCount() {
        return count;
    }

    public void service(ServletRequest request, ServletResponse response) {
        BigInteger i = extractFromRequest(request);
        BigInteger[] factors = factor[i.intValue()];
        ++count; //读取-修改-写入，其结果依赖于之前的状态
        encodeIntoResponse(response, factors);
    }

}
