package com.thread.chapter02.demo1;

import javax.servlet.*;
import java.io.IOException;
import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 对最近计算的结果进行缓存
 * 存在多个状态变量，lastNumber的值和lastFactors的值之间没有约束，非原子性，线程不安全
 */
public class UnsafeCachingFactorizer extends MyAbstractServlet {
    //最近访问的序号
    private final AtomicReference<BigInteger> lastNumber = new AtomicReference<>();
    //最近访问的结果
    private final AtomicReference<BigInteger[]> lastFactors = new AtomicReference<>();

    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        BigInteger i = extractFromRequest(request);
        if (i.equals(lastNumber.get())) {
            encodeIntoResponse(response, lastFactors.get());
        } else {
            BigInteger[] factors = factor[i.intValue()];
            lastNumber.set(i);
            lastFactors.set(factors);
            encodeIntoResponse(response, factors);
        }
    }

}
