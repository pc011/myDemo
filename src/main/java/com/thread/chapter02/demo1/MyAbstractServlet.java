package com.thread.chapter02.demo1;

import javax.servlet.*;
import java.io.IOException;
import java.math.BigInteger;

public abstract class MyAbstractServlet implements Servlet {

    protected BigInteger[][] factor;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {

    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public abstract void service(ServletRequest request, ServletResponse response) throws ServletException, IOException;

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {

    }

    protected BigInteger extractFromRequest(ServletRequest request) {
        return null;
    }

    protected void encodeIntoResponse(ServletResponse response, BigInteger[] factors) {

    }
}
