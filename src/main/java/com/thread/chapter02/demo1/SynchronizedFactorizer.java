package com.thread.chapter02.demo1;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.math.BigInteger;

/**
 * 使用同步的方式保证正确缓存最新的计算结果，并发性非常差，会导致活跃性问题或性能问题。
 *
 * 当某个变量由锁来保护时，意味着每次访问这个变量都需要获得锁，以确保同一个时刻只有同一个线程访问这个变量，
 * 当类的不变性条件涉及多个状态变量时，在不变性条件中的每个变量都必须由同一个锁来保护。
 * 因此可以在单个原子操作中访问或者更新这些变量，从而确保不变性条件不被破坏
 */
public class SynchronizedFactorizer extends MyAbstractServlet {

    private BigInteger lastNumber;
    private BigInteger[] lastFactors;

    @Override
    public synchronized void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        BigInteger i = extractFromRequest(request);
        if (i.equals(lastNumber)) {
            encodeIntoResponse(response, lastFactors);
        } else {
            BigInteger[] factors = factor[i.intValue()];
            lastNumber = i;
            lastFactors = factors;
            encodeIntoResponse(response, factors);
        }
    }

}
