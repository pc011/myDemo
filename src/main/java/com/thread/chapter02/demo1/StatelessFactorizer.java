package com.thread.chapter02.demo1;

import javax.servlet.*;
import java.math.BigInteger;

/**
 * 无状态的Servlet示例
 * 无状态（不包含任何域和对其它类的域的引用）的对象一定是线程安全的
 */
public class StatelessFactorizer extends MyAbstractServlet {

    public void service(ServletRequest request, ServletResponse response) {
        BigInteger i = extractFromRequest(request);
        BigInteger[] factors = factor[i.intValue()];
        encodeIntoResponse(response, factors);
    }

}
