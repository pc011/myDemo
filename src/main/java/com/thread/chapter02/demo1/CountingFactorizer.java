package com.thread.chapter02.demo1;

import javax.servlet.*;
import java.io.IOException;
import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 使用Atomic类型的变量来统计已处理的请求数、
 * Servlet的状态就是计数器的状态，计数器是线程安全的，所以Servlet是线程安全的
 */
public class CountingFactorizer extends MyAbstractServlet {

    private AtomicLong count = new AtomicLong(0);

    public long getCount() {
        return count.get();
    }

    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        BigInteger i = extractFromRequest(request);
        BigInteger[] factors = factor[i.intValue()];
        count.incrementAndGet();
        encodeIntoResponse(response, factors);
    }
}
