package com.thread.chapter02.demo1;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.math.BigInteger;

/**
 * 缓存最近计算的数值和其计算结果的Servlet
 * 缩小同步代码块的作用范围，实现简单性和并发性之间的平衡
 */
public class CachingFactorizer extends MyAbstractServlet {

    private BigInteger lastNumber;
    private BigInteger[] lastFactors;
    private long hits;
    private long cacheHits;

    public synchronized long getHits() {
        return hits;
    }

    public synchronized double getCacheHitRatio() {
        return (double)cacheHits / (double)hits;
    }

    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        BigInteger i = extractFromRequest(request);
        BigInteger[] factors = null;
        //同步1 保护“先检查后执行”的操作序列，保证hits和cacheHits两个状态的同步更新
        synchronized (this) {
            ++hits;
            if (i.equals(lastNumber)) {
                ++cacheHits;
                factors = lastFactors.clone();
            }
        }
        //同步2 保证lastNumber和lastFactors两个状态的同步更新
        if (factors == null) {
            factors = factor[i.intValue()];
            synchronized (this) {
                lastNumber = i;
                lastFactors = factors.clone();
            }
        }
        encodeIntoResponse(response, factors);

    }
}
