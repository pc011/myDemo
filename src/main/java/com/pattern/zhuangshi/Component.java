package com.pattern.zhuangshi;

/**
 * 装饰模式 - 抽象构件
 */
public abstract class Component {

    public abstract void operaton();

}
