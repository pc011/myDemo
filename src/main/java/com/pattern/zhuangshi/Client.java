package com.pattern.zhuangshi;

public class Client {

    public static void main(String[] args) {

        Component component = new ConcreateComponent();
        System.out.println("装饰前");
        component.operaton();
        System.out.println("装饰后");
        component = new ConcrateDecorator(component);
        component.operaton();

    }

}
