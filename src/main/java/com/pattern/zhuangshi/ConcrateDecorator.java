package com.pattern.zhuangshi;

public class ConcrateDecorator extends Decorator {

    public ConcrateDecorator(Component component) {
        super(component);
    }

    @Override
    public void operaton() {
        super.operaton();
        method1();
    }

    private void method1() {
        System.out.println("invoke concrateDecorator method1");
    }
}
