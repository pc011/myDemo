package com.pattern.zhuangshi;

public abstract class Decorator extends Component {

    private Component component;

    public Decorator(Component component) {
        this.component = component;
    }

    @Override
    public void operaton() {
        System.out.println("invoke decorator.operaton...");
        this.component.operaton();
    }
}
