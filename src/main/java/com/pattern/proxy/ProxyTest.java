package com.pattern.proxy;

public class ProxyTest {

    public static void main(String[] args) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Person target = new PengChong();



        Person person = (Person) new HouseSaler().getInstance(target);
        Class clazz2 = null;
        try {
            clazz2 = classLoader.loadClass("com.sun.proxy.$Proxy0");
            System.out.println("加载class2成功:"+clazz2.getName());
        } catch (ClassNotFoundException e) {
            System.out.println("加载class2失败");
        }

        System.out.println("Person.class: " + person.getClass().getName());

        Person person2 = (Person) new HouseSaler().getInstance(target);
        System.out.println("Person2.class: " + person2.getClass().getName());

//        toString(person.getClass().getInterfaces());
//        toString(person.getClass().getDeclaredClasses());
//        toString(person.getClass().getDeclaredConstructors());
//        toString(person.getClass().getDeclaredFields());
//        toString(person.getClass().getDeclaredMethods());
        System.out.println(target.equals(person));

        Class clazz3 = null;
        try {
            clazz3 = classLoader.loadClass("com.sun.proxy.$Proxy0");
            System.out.println("加载class3成功:"+clazz3.getName());
        } catch (ClassNotFoundException e) {
            System.out.println("加载class3失败");
        }

        person.buyHouse();

    }

    public static void toString(Object[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i].toString());
        }

    }


}
