package com.pattern.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

public class HouseSaler implements InvocationHandler {

    private Person target;

    public Object getInstance(Person person) {
        this.target = person;
        Class[] interfaces = new Class[1];
        interfaces[0] = Person.class;
        return Proxy.newProxyInstance(person.getClass().getClassLoader(), interfaces, this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("我先带你看房");
        this.target.buyHouse();
        System.out.println("买完了付钱吧");
        return null;
    }

}
