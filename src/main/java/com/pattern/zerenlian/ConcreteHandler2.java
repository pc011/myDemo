package com.pattern.zerenlian;

public class ConcreteHandler2 extends Handler {

    @Override
    protected Integer getHandlerLevel() {
        return 2;
    }

    @Override
    protected Response echo(Request request) {
        System.out.println("ConcreteHandler2 echo...");
        return new Response();
    }
}
