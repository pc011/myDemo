package com.pattern.zerenlian;

public class ConcreteHandler1 extends Handler {

    @Override
    protected Integer getHandlerLevel() {
        return 1;
    }

    @Override
    protected Response echo(Request request) {
        System.out.println("ConcreteHandler1 echo...");
        return new Response();
    }
}
