package com.pattern.zerenlian;

/**
 * 责任链模式
 */
public abstract class Handler {

    private Handler nextHandler;

    //每个处理者都必须对请求做出响应
    public final Response handleMessage(Request request) {
        Response response = null;
        if (this.getHandlerLevel().equals(request.getRequestLevel())) {
            response = this.echo(request);
        } else {
            if (this.nextHandler != null) {
                response = this.nextHandler.handleMessage(request);
            } else {
                throw new RuntimeException("没有适合的处理者");
            }
        }

        return response;
    }

    //设置下一个处理者
    public void setNext(Handler handler) {
        this.nextHandler = handler;
    }

    //获取当前处理者的级别
    protected abstract Integer getHandlerLevel();

    //每个矗立着都必须实现处理任务
    protected abstract Response echo(Request request);

}
