package com.pattern.zerenlian;

public class ConcreteHandler3 extends Handler {

    @Override
    protected Integer getHandlerLevel() {
        return 3;
    }

    @Override
    protected Response echo(Request request) {
        System.out.println("ConcreteHandler3 echo...");
        return new Response();
    }
}
