package com.pattern.zerenlian;

public class Request {

    private int requestLevel;

    public Integer getRequestLevel() {
        return requestLevel;
    }

    public void setRequestLevel(int requestLevel) {
        this.requestLevel = requestLevel;
    }
}
