package com.pattern.cglibproxy;

public class CGLibProxyTest {

    public static void main(String[] args) {

        PengChong pengChong = (PengChong) new HouseSaler().getInstance(PengChong.class);
        pengChong.buyHouse();

    }

}
