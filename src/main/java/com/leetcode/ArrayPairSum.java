package com.leetcode;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayPairSum {

    public static void main(String[] args) {
        int[] arr = {4,9,2,1,4,3,6,7,1,9};
        int i = new ArrayPairSum().arrayPairSum(arr);
        System.out.println(i);
    }

    public int arrayPairSum(int[] nums) {
        if (nums.length%2 == 1) {
            return 0;
        }
        sort(nums, 0, nums.length-1);
        int sum = 0;
        for (int i = 0; i < nums.length; i+=2) {
            sum += nums[i];
        }
        return sum;
    }

    public int partition(int []array,int lo,int hi){
        //固定的切分方式
        int key=array[lo];
        while(lo<hi){
            while(array[hi]>=key&&hi>lo){//从后半部分向前扫描
                hi--;
            }
            array[lo]=array[hi];
            while(array[lo]<=key&&hi>lo){//从前半部分向后扫描
                lo++;
            }
            array[hi]=array[lo];
        }
        array[hi]=key;
        return hi;
    }

    public void sort(int[] array,int lo ,int hi){
        if(lo>=hi){
            return ;
        }
        int index=partition(array,lo,hi);
        sort(array,lo,index-1);
        sort(array,index+1,hi);
    }

}
