package com.clazz.demo1;

public class TestClass {
    private static int b1;
    private int b2;

    public static class InnerStaticClass {
        private int a;
        private static int b;
        public void test(){
            b1 = 1;
        }
    }

    class InnerNormalClass {
        private int a;

        public InnerNormalClass(){
            b2 = 1;
            b1 = 1;
        }
        //private static int b;
    }

    private static int n;

    private int m;

    public int addM() {
        return m+1;
    }

    public static void main(String[] args) {
        TestClass testClass = new TestClass();
        InnerStaticClass innerClass2 = new TestClass.InnerStaticClass();
        InnerNormalClass normalClass = testClass.new InnerNormalClass();
    }

    public void test() {
        TestClass testClass = new TestClass();
        InnerStaticClass innerStaticClass = new InnerStaticClass();
        InnerNormalClass  innerClass2 = new TestClass.InnerNormalClass();
    }

}
