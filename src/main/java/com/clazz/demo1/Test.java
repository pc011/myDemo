package com.clazz.demo1;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test {

    public static void main(String[]  args ) {
        testFixed();
    }

    //输出结果 第1个线程pool-1-thread-1 第2个线程pool-1-thread-1 第3个线程pool-1-thread-1 第4个线
    public static void testCache() {
        ExecutorService cacheThreadPool = Executors.newCachedThreadPool();
        for(int i =1;i<=10;i++){
            final int index=i ;
            try{
                Thread.sleep(1000);
            }catch(InterruptedException  e ) {
                e.printStackTrace();
            }
            cacheThreadPool.execute(new Runnable(){
                @Override
                public void run() {
                    System.out.println("第" +index +"个线程" +Thread.currentThread().getName());
                }
            });
        }
    }

    public static void testFixed() {

        ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 10; i++) {
            final int index = i;
            fixedThreadPool.execute(new Runnable() {
                public void run() {
                    try {
                        System.out.println("第" +index +"个线程" +Thread.currentThread().getName());
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}

