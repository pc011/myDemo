package com.base._volatile;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * volatile保证可见性并禁止指令重排，synchronized和lock保证操作原子性
 */
public class VolatileTest {

    Lock lock = new ReentrantLock();

    private volatile int inc = 0;

    private AtomicInteger ainc = new AtomicInteger(0);

    public void increase() {
        inc++;
    }

    public synchronized void increase1() {
        inc++;
    }

    public void increase2() {
        lock.lock();
        try {
            //自增操作非原子性
            inc++;
        } finally {
            lock.unlock();
        }
    }

    public void increase3() {
        ainc.getAndIncrement();
    }
    public static void main(String[] args) {
        final VolatileTest volatileTest = new VolatileTest();
        for (int i = 0; i < 10; i++) {
            new Thread(new Runnable() {
                public void run() {
                    for (int j = 0; j < 1000; j++) {
                        volatileTest.increase1();
                    }
                }
            }).start();
        }
        while (Thread.activeCount() > 1) {
            Thread.yield();
        }
        System.out.println("inc : "+volatileTest.inc);
    }

}
