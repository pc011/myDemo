package com.base._volatile;

public class VolatileTest2 {

    private volatile boolean isFinish = false;

    public static void main(String[] args) {
        final VolatileTest2 volatileTest2 = new VolatileTest2();
        Thread thread1 = new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(5000);
                    System.out.println("thread1 will finish");
                    volatileTest2.isFinish = true;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread1.start();
        while (volatileTest2.isFinish == false) {
            Thread.yield();
        }
        System.out.println("thread1 is finish");
    }

}
