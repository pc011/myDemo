package com.base._static;

/**
 */
public class Outer {

    private int age;

    private String name;

    public static class Builder {
        int age;
        String name;

        public Builder(int age) {
            this.age = age;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Outer build(){
            return new Outer(this);
        }

    }

    private Outer(Builder builder) {
        this.age = builder.age;
        this.name = builder.name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
