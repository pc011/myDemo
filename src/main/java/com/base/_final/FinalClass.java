package com.base._final;

/**
 */
public class FinalClass {

    private final int a;

    public FinalClass(int a) {
        this.a = a;
    }

    public final void setA(int a) {

    }

    public void setB(int a) {

    }

}
