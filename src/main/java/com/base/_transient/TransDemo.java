package com.base._transient;

import java.io.*;

/**
 * 声明为transist的实例变量，在序列化时，该变量不会持久化，在反序列化时，值不会被恢复
 */

public class TransDemo {
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.err.println("usage: java TransDemo classfile");
            return;
        }
        ClassLib cl = new ClassLib(new FileInputStream(args[0]));
        System.out.printf("Minor version number: "+cl.getMinorVer());
        System.out.printf("Major version number: "+cl.getMajorVer());
        cl.showIS();

        cl = null;

        try {
            FileOutputStream fos = new FileOutputStream("x.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(cl);
            oos.close();
            fos.close();

            cl = null;

            FileInputStream fis = new FileInputStream("x.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            System.out.println();
            cl = (ClassLib) ois.readObject();
            System.out.printf("Minor version number: "+cl.getMinorVer());
            System.out.printf("Major version number: "+cl.getMajorVer());
            cl.showIS();

            fis.close();
            ois.close();
        } catch (ClassNotFoundException cnfe) {
            System.err.println(cnfe.getMessage());
        }
    }
}